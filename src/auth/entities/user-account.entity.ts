import { Column, DeepPartial, Entity } from 'typeorm';
import { Thing } from '../../common';

/**
 *
 * @enum UserAccountTypeEnum
 * @description Holds the list for the available user accounts
 *
 * @property {string} Local Declares that the UserAccount is of UserAccountLocal type
 *
 */
export enum UserAccountTypeEnum {
  Local = 'local',
}

/**
 *
 * @class UserAccount
 * @description Declares an authentication method for a user
 *
 * @property {string} additionalType A descriptor of what is the inner contents of this model
 * @property {string} email The email that is associated with that account
 * @property {boolean} isActive Describes whether the authentication method is active by the user
 * @property {boolean} isVerified Describes whether the authentication email is verified
 * @property {number} userId Declares the user id that this account refers to. Note that the there is no SQL relationship and/or constraint
 *
 */
@Entity({
  name: 'Auth_UserAccount',
})
export class UserAccount extends Thing {
  constructor(params: DeepPartial<UserAccount>) {
    super(params);

    if (params) {
      this.additionalType = params.additionalType;
      this.email = params.email;
      this.isActive = params.isActive;
      this.isVerified = params.isVerified;
      this.userId = params.userId;
    }
  }

  @Column({
    nullable: false,
    type: 'enum',
    enum: UserAccountTypeEnum,
    default: UserAccountTypeEnum.Local,
  })
  additionalType: UserAccountTypeEnum;

  @Column({
    nullable: false,
    unique: true,
  })
  email: string;

  @Column({
    nullable: false,
    default: true,
  })
  isActive: boolean;

  @Column({
    nullable: false,
    default: false,
  })
  isVerified: boolean;

  @Column({
    nullable: false,
    type: 'int',
  })
  userId: number;
}
