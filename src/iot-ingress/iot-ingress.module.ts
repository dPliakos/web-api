import { Module } from '@nestjs/common';
import { CommonModule } from '../common';
import { IotDeviceService } from './services/iot-device/iot-device.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IoTDeviceEntity } from './entities/iot-device.entity';
import { IoTDeviceTokenEntity } from './entities/iot-device-token.entity';
import { IotAuthGuard } from './guards/iot-auth.guard';
import { IotIngressController } from './iot-ingress.controller';
import { IotIngressService } from './services/iot-ingress.service';
import { TimeSeriesModule } from '../time-series/time-series.module';
import { SensorEntity } from './entities/sensor.entity';
import { SensorService } from './services/sensor/sensor.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { IConfiguration } from '../config/config.inteface';
import { SensorMeasurementService } from './services/sensor-measurement/sensor-measurement.service';
import { SensorMeasurementEntity } from './entities/sensor-measurement.entity';
import { IoTActuatorEntity } from './entities/iot-actuator.entity';
import { IoTActuatorService } from './services/iot-actuator/iot-actuator.service';
import { IoTActuatorEventHandlerService } from './services/iot-actuator/iot-actuator-event-handler.service';
import { SensorMeasurementEventHandlerService } from './services/sensor-measurement/sensor-measurement.event-handler.service';
import { RabbitMQService } from './services/rabbitmq/rabbitmq.service';
import { HttpModule } from '@nestjs/axios';
import { RabbitmqPublisherService } from './services/rabbitmq/rabbitmq-publisher.service';

let a;
@Module({
  imports: [
    ConfigModule,
    CommonModule,
    HttpModule,
    TypeOrmModule.forFeature([
      IoTDeviceEntity,
      IoTDeviceTokenEntity,
      SensorEntity,
      SensorMeasurementEntity,
      IoTActuatorEntity,
    ]),
    TimeSeriesModule.registerAsync({
      useFactory: (configService: ConfigService) => {
        const influxDbOptions: IConfiguration['influx'] =
          configService.get('influx');
        return {
          url: influxDbOptions.url,
          org: influxDbOptions.org,
          bucket: influxDbOptions.bucket,
          token: influxDbOptions.token,
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [
    IotDeviceService,
    IotAuthGuard,
    IotIngressService,
    SensorService,
    SensorMeasurementService,
    IoTActuatorService,
    IoTActuatorEventHandlerService,
    SensorMeasurementEventHandlerService,
    RabbitMQService,
    RabbitmqPublisherService,
  ],
  controllers: [IotIngressController],
})
export class IoTIngressModule {
  constructor(private readonly _configService: ConfigService) {
    try {
      // setImmediate(() => a= new RabbitMQService(this._configService))
    } catch (err) {
      console.error(err);
    }
  }
}
