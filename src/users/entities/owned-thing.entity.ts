import { EntityValidationResult, Thing } from '../../common';
import { User } from './user.entity';
import { DeepPartial, ManyToOne } from 'typeorm';
import { JoinTable } from 'typeorm';

/**
 *
 * @class OwnedThing
 * @description Describes an entity that is owned by a user
 *
 * @property {User} owner The owner of the entity
 *
 */
export class OwnedThing extends Thing {
  constructor(props?: DeepPartial<OwnedThing>) {
    super(props);

    if (props) {
      this.owner = props.owner as User;
    }
  }

  @ManyToOne(() => User, {
    nullable: false,
  })
  @JoinTable()
  owner: User;

  validate(): EntityValidationResult {
    const validation = super.validate();

    if (!this.owner) {
      validation.validationErrors.push(`Property 'owner' must be defined`);
    }

    return {
      validationErrors: validation.validationErrors,
      isValid: validation.validationErrors.length === 0,
    };
  }
}
