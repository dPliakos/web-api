import { IotDeviceService } from '../iot-device/iot-device.service';
import { SensorService } from '../sensor/sensor.service';
import { IotActuatorService } from '../iot-actuator/iot-actuator.service';
import { SensorLabelService } from '../sensor-label/sensor-label.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class StatisticsService {
  constructor(
    private readonly _iotDevicesService: IotDeviceService,
    private readonly _sensorsService: SensorService,
    private readonly _actuatorService: IotActuatorService,
    private readonly _labelService: SensorLabelService,
  ) {}

  async getActiveUserInstances(ownerId: number) {
    const ownerQuery = {
      where: {
        owner: {
          id: ownerId,
        },
      },
    };

    const sensorsQuery = this._sensorsService.count(ownerQuery);
    const actuatorsQuery = this._actuatorService.count(ownerQuery);
    const labelsQuery = this._labelService.count(ownerQuery);
    const devicesQuery = this._iotDevicesService.count(ownerQuery);

    const [devices, sensors, actuators, labels] = await Promise.all([
      devicesQuery,
      sensorsQuery,
      actuatorsQuery,
      labelsQuery,
    ]);

    return {
      devices,
      sensors,
      actuators,
      labels,
    };
  }
}
