import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Logger,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  BaseController,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  IoTValue,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import { IoTAlertConfigurationEntity } from '../../entities/iot-alert-configuration.entity';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import { IoTTaskService } from '../../../iot-tasks';
import { IoTTaskCheckThresholdService } from '../../../iot-tasks/services/iot-check-threshold/iot-task-check-threshold.service';
import { IotActuatorTriggerConfigurationEntity } from 'src/iot-domain/entities/iot-actuator-trigger-configuration.entity';
import { IotActuatorTriggerConfigurationService } from 'src/iot-domain/services/iot-actuator-trigger-configuration/iot-actuator-trigger-configuration.service';
import {
  IoTActuatorTriggerConfigurationCreateDTO,
  IoTActuatorTriggerConfigurationQueryDTO,
  IoTActuatorTriggerConfigurationUpdateDTO,
} from 'src/iot-domain/dto/iot-actuator-trigger-configuration.dto';
import { IoTActuatorEntity } from 'src/iot-domain/entities/iot-actuator.entity';
import { IotActuatorService } from 'src/iot-domain/services/iot-actuator/iot-actuator.service';
import { IoTAlertConfigurationService } from 'src/iot-domain/services/iot-alert-configuration/iot-alert-configuration.service';

@Controller('iot-actuator-trigger-configuration')
@ApiTags('iot-actuator-trigger-configuration')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class IotActuatorTriggerConfigurationController extends BaseController<IotActuatorTriggerConfigurationEntity> {
  constructor(
    protected readonly _iotActuatorTriggerConfigurationService: IotActuatorTriggerConfigurationService,
    protected readonly _queryService: QueryService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _iotActuatorService: IotActuatorService,
    private readonly _iotTaskService: IoTTaskService,
    private readonly _iotAlertConfigurationService: IoTAlertConfigurationService,
    private readonly _iotTaskCheckThesholdService: IoTTaskCheckThresholdService,
  ) {
    super(
      _iotActuatorTriggerConfigurationService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(`IoTDomain/${IotActuatorTriggerConfigurationService.name}`),
    );
  }

  @Get('/')
  async getUserActuatorTriggerConfigurations(
    @Request() req,
    @Query() filtersQuery: IoTActuatorTriggerConfigurationQueryDTO,
    @Query() pagination: PaginationQueryDto,
  ): Promise<IResponse<IotActuatorTriggerConfigurationEntity[]>> {
    const { take, skip, page, perPage } =
      this._queryService.resolvePagination(pagination);

    const { searchFactors } = this._queryService.resolveSearchQuery(
      filtersQuery,
      ['parent', 'alert'],
    );

    const whereClause = {
      owner: {
        id: req.user.id,
        deletedDate: null,
      },
      identifier: filtersQuery?.identifier,
    };

    const whereClauseWithSearch =
      this._iotActuatorTriggerConfigurationService.formatFindOptionsWithSearch(
        whereClause,
        searchFactors,
      );
    const [triggerConfigurations, triggerConfigurationCount] =
      await this._iotActuatorTriggerConfigurationService.findAndCount({
        take,
        skip,
        where: whereClauseWithSearch,
        relations: ['parent', 'alert'],
        order: {
          createdAt: 'DESC',
        },
      });

    return {
      data: triggerConfigurations,
      total: triggerConfigurationCount,
      page,
      perPage,
    };
  }

  @Get('/:identifier')
  async getOne(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<IotActuatorTriggerConfigurationEntity>> {
    const result = await super.getOne(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      order: {
        createdAt: 'DESC',
      },
      relations: ['owner', 'parent', 'alert'],
    });

    return result;
  }

  @Post()
  async create(
    @Request() req,
    @Body() dto: IoTActuatorTriggerConfigurationCreateDTO,
  ): Promise<IResponse<IotActuatorTriggerConfigurationEntity>> {
    let parent: IoTActuatorEntity;
    let alert: IoTAlertConfigurationEntity;

    try {
      parent = await this._iotActuatorService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.parent,
        },
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!parent) {
      const err = new NotFoundException(`Actuator ${dto.parent} was not found`);
      throw this.getThrowable(err, err);
    }

    if (
      !this.isValueWithinBounds(
        dto.targetValue,
        parent.lowerLimit,
        parent.upperLimit,
      )
    ) {
      const err = new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Bad Request',
          message: ["targetValue must be within the actuator's allowed range"],
        },
        HttpStatus.BAD_REQUEST,
      );

      throw this.getThrowable(err, err);
    }

    if (parent.valueType !== parent.valueType) {
      const err = new TypeError(
        `Actuator ${dto.parent} accepts values of type ${
          parent.valueType
        } but a target value of type ${typeof dto.targetValue} was provided`,
      );
      throw this.getThrowable(err, err);
    }

    try {
      alert = await this._iotAlertConfigurationService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.alert,
        },
        relations: ['task', 'sensor'],
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!alert) {
      const err = new NotFoundException(
        `IoTAlertConfiguration ${dto.alert} was not found`,
      );
      throw this.getThrowable(err, err);
    }

    const actuatorTriggerConfiguration =
      await this._iotActuatorTriggerConfigurationService.create({
        name: dto.name,
        owner: req.user,
        parent: parent,
        alert: alert,
        triggerState: dto.triggerState,
        targetValue: dto.targetValue,
        isActive: dto.isActive,
      });

    const configuration =
      await this._iotActuatorTriggerConfigurationService.save(
        actuatorTriggerConfiguration,
      );

    return {
      data: configuration,
    };
  }

  @Put(':identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: IoTActuatorTriggerConfigurationUpdateDTO,
  ) {
    let configuration: IotActuatorTriggerConfigurationEntity;
    let parent: IoTActuatorEntity;
    let alert: IoTAlertConfigurationEntity;

    try {
      configuration =
        await this._iotActuatorTriggerConfigurationService.findOne({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: params.identifier,
          },
          relations: ['owner'],
        });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!configuration) {
      const error = new NotFoundException(
        `IotActuatorTriggerConfiguration ${params.identifier} was not found`,
      );
      throw this.getThrowable(error, error);
    }

    try {
      parent = await this._iotActuatorService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.parent,
        },
        relations: ['owner'],
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!parent) {
      const err = new NotFoundException(`Actuator ${dto.parent} was not found`);
      throw this.getThrowable(err, err);
    }

    try {
      alert = await this._iotAlertConfigurationService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.alert,
        },
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!alert) {
      const err = new NotFoundException(`Actuator ${dto.alert} was not found`);
      throw this.getThrowable(err, err);
    }

    if (
      !this.isValueWithinBounds(
        dto.targetValue,
        parent.lowerLimit,
        parent.upperLimit,
      )
    ) {
      const err = new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Bad Request',
          message: ["targetValue must be within the actuator's allowed range"],
        },
        HttpStatus.BAD_REQUEST,
      );

      throw this.getThrowable(err, err);
    }

    const updatableFields = this.filterUpdatableFields(dto, [
      'name',
      'triggerState',
      'targetValue',
      'isActive',
    ]);

    let updatedConfiguration;

    try {
      updatedConfiguration = {
        ...configuration,
        owner: req.user,
        parent,
        alert,
        ...updatableFields,
      };

      await this._iotActuatorTriggerConfigurationService.save(
        updatedConfiguration,
      );
      updatedConfiguration =
        await this._iotActuatorTriggerConfigurationService.findOne({
          where: {
            identifier: configuration.identifier,
          },
          relations: ['owner', 'alert', 'parent'],
        });
    } catch (err) {
      throw this.getThrowable(err);
    }

    return {
      data: updatedConfiguration,
    };
  }

  @Delete(':identifier')
  async remove(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.remove(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
    });
  }

  isValueWithinBounds(
    targetValue: IoTValue,
    lowerBound?: number,
    upperBound?: number,
  ): boolean {
    if (typeof targetValue === 'number') {
      // Numeric value - check if within bounds
      if (lowerBound !== undefined && targetValue < lowerBound) {
        return false;
      }
      if (upperBound !== undefined && targetValue > upperBound) {
        return false;
      }
    }
    return true;
  }
}
