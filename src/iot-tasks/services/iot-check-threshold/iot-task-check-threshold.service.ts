import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { EntityService } from '../../../common';
import { IoTTaskCheckThresholdEntity } from '../../entities';

@Injectable()
export class IoTTaskCheckThresholdService extends EntityService<IoTTaskCheckThresholdEntity> {
  constructor(
    @InjectRepository(IoTTaskCheckThresholdEntity)
    private readonly _repository: Repository<IoTTaskCheckThresholdEntity>,
  ) {
    super(IoTTaskCheckThresholdService.name);
    this.repository = _repository;
  }

  async create(item) {
    const entity = new IoTTaskCheckThresholdEntity(item);
    const validation = entity.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    return entity;
  }

  async save(
    item: DeepPartial<IoTTaskCheckThresholdEntity>,
  ): Promise<IoTTaskCheckThresholdEntity> {
    return super.save(item);
  }

  async createAndSave(
    item: DeepPartial<IoTTaskCheckThresholdEntity>,
  ): Promise<IoTTaskCheckThresholdEntity> {
    const instance = await this.create(item);
    return this.save(instance);
  }
}
