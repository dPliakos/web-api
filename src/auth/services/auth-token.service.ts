import { EntityService } from '../../common';
import { AuthTokenEntity } from '../entities/auth-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

export class AuthTokenService extends EntityService<AuthTokenEntity> {
  constructor(
    @InjectRepository(AuthTokenEntity)
    private readonly _resetTokenRepository: Repository<AuthTokenEntity>,
  ) {
    super(AuthTokenService.name);
    this.repository = _resetTokenRepository;
  }

  async save(entity: Partial<AuthTokenEntity>) {
    if (!entity.expirationDate) {
      const expirationDate = new Date();
      expirationDate.setHours(expirationDate.getHours() + 1);
      entity.expirationDate = expirationDate;
    }

    const token = new AuthTokenEntity(entity);

    return super.save({
      ...token,
    });
  }
}
