import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface IUserCreatedEventPayload
 * @description The payload of a IUserCreatedEvent
 *
 * @property {number} id The public identifier of the User
 * @property {string} email The user email
 *
 */
interface IUserCreatedEventPayload extends IApplicationEventPayload {
  id: number;
  email: string;
}

/**
 *
 * @interface IUserCreatedEvent
 * @description Describes the creation of a user
 *
 * @property {string} subject The event subject
 * @property {ISensorLabelRemovedEventPayload} payload
 *
 */
export interface IUserCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.UserCreated;
  payload: IUserCreatedEventPayload;
}
