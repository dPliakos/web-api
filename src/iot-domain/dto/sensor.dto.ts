import {
  IsArray,
  IsIn,
  IsNotEmpty,
  IsOptional,
  IsUUID,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ResourceSearchQueryDTO } from '../../common';

/**
 *
 * @class CreateSensorInputDTO
 * @description Describes an input object for an iot sensor
 *
 * @property {string} name The name of the sensor
 * @property {string} description A descriptive message for the sensor
 * @property {string} valueType The type of the values that the sensor is posing
 * @property {string} measurementUnit The unit in which sensor measurements are collected
 * @property {string} parentIoTDevice The identifier of the parent IoT Device
 *
 */
export class CreateSensorInputDTO {
  @ApiProperty()
  @IsNotEmpty()
  @MaxLength(25)
  @MinLength(2)
  name: string;

  @ApiProperty({
    nullable: true,
  })
  @MaxLength(250)
  @IsOptional()
  description?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  measurementUnit: string;

  @ApiProperty()
  @IsIn(['int', 'float', 'boolean', 'string'])
  valueType?: 'int' | 'float' | 'boolean' | 'string';

  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  parentIoTDevice: string;
}

export class SensorDTO {
  name: string;
  description: string;
  valueType: string;
  measurementUnit: string;
  parentIoTDevice: string;
}

export class UpdateSensorInputDTO {
  @ApiProperty()
  @MaxLength(25)
  @MinLength(2)
  @IsOptional()
  name?: string;

  @ApiProperty()
  @MaxLength(250)
  @IsOptional()
  description?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  @IsOptional()
  measurementUnit?: string;

  @ApiProperty()
  @IsOptional()
  parentDevice?: {
    identifier: string;
  };

  @ApiProperty()
  @IsOptional()
  @IsArray()
  labels?: string[];
}

export class SensorFilterQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty()
  @IsUUID()
  @IsOptional()
  identifier?: string;

  @ApiProperty()
  @IsUUID()
  @IsOptional()
  device?: string;

  @ApiProperty()
  @IsOptional()
  measurementUnit: string;

  @ApiProperty()
  @IsOptional()
  valueType?: 'string' | 'boolean' | 'int' | 'float';

  @ApiProperty()
  @IsOptional()
  @IsArray()
  labels?: string[];

  @ApiProperty()
  @IsOptional()
  @IsUUID()
  label?: string;
}

export class SensorRelatedLabelDTO {
  @ApiProperty()
  @IsUUID()
  label: string;
}
