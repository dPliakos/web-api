import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class PaginationQueryDto {
  @ApiProperty({
    required: false,
    default: 1,
  })
  @IsOptional()
  page?: number;

  @ApiProperty({
    required: false,
    default: 10,
  })
  @IsOptional()
  perPage?: number;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  filter?: string;
}
