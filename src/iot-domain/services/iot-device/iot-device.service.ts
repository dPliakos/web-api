import { Injectable, Logger } from '@nestjs/common';
import {
  EntityService,
  IIoTDeviceCreatedEvent,
  IIoTDeviceSoftRemovedEvent,
  ApplicationEventSubject,
} from '../../../common';
import { IotDeviceEntity } from '../../entities/iot-device.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, FindOptionsWhere, Repository } from 'typeorm';
import { ApplicationEventsService } from '../../../common/services/application-events.service';
import { IoTDeviceEntity } from '../../../iot-ingress/entities/iot-device.entity';
import { RabbitmqService } from '../rabbitmq/rabbitmq.service';

@Injectable()
export class IotDeviceService extends EntityService<IotDeviceEntity> {
  private readonly _logger: Logger = new Logger(IotDeviceService.name);

  constructor(
    @InjectRepository(IotDeviceEntity)
    private readonly iotDeviceEntityRepository: Repository<IotDeviceEntity>,
    private readonly _applicationEventsService: ApplicationEventsService,
    private readonly _rabbitMqService: RabbitmqService,
  ) {
    super(IotDeviceEntity.name);
    this.repository = iotDeviceEntityRepository;
  }

  async save(item: DeepPartial<IotDeviceEntity>) {
    const result = await super.save(item);
    const event: IIoTDeviceCreatedEvent = {
      subject: ApplicationEventSubject.IoTDeviceCreated,
      payload: {
        identifier: result.identifier,
        ownerId: result.owner.id,
      },
    };

    this._applicationEventsService.emit(event);
    this._rabbitMqService
      .compareStates()
      .catch((err) => this._logger.error(err));
    return result;
  }

  async softRemove(id: number) {
    const device = await this.iotDeviceEntityRepository.findOne({
      where: {
        id: id,
      },
    });
    const result = await super.softRemove(device.id);

    if (result.affected > 0) {
      const event: IIoTDeviceSoftRemovedEvent = {
        subject: ApplicationEventSubject.IoTDeviceSoftRemoved,
        payload: {
          identifier: device.identifier,
        },
      };

      this._applicationEventsService.emit(event);
    }
    return result;
  }
}
