/**
 *
 * @interface IJWTTokenData
 * @description Describes the data that the token holds
 *
 * @property {string} email The email for which the token was issued
 * @property {string} identifier The user identifier for which the token was issued
 *
 */
export interface IJWTTokenData {
  email: string;
  identifier: string;
}
