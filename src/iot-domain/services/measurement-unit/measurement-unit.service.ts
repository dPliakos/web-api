import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MeasurementUnitEntity } from '../../entities/measurement-unit.entity';
import { Repository } from 'typeorm';
import { EntityService } from '../../../common';

@Injectable()
export class MeasurementUnitService extends EntityService<MeasurementUnitEntity> {
  constructor(
    @InjectRepository(MeasurementUnitEntity)
    private readonly _measurementUnitEntity: Repository<MeasurementUnitEntity>,
  ) {
    super(MeasurementUnitEntity.name);
    this.repository = _measurementUnitEntity;
  }
}
