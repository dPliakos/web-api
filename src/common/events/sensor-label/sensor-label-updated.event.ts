import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface ISensorLabelUpdatedEventPayload
 * @description The payload of a ISensorLabelUpdatedEvent
 *
 * @property {string} label The public identifier of the SensorLabel
 * @property {string} name The name of the SensorLabel
 *
 */
interface ISensorLabelUpdatedEventPayload extends IApplicationEventPayload {
  label: string;
  name: string;
}

/**
 *
 * @interface ISensorLabelCreatedEvent
 * @description Describes the creation of a SensorLabel
 *
 * @property {string} subject The event subject
 *
 */
export interface ISensorLabelUpdatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorLabelUpdated;
  payload: ISensorLabelUpdatedEventPayload;
}
