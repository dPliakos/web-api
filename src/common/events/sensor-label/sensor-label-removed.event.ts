import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface ISensorLabelRemovedEventPayload
 * @description The payload of a ISensorLabelRemovedEvent
 *
 * @property {string} label The public identifier of the SensorLabel
 *
 */
interface ISensorLabelRemovedEventPayload extends IApplicationEventPayload {
  label: string;
}

/**
 *
 * @interface ISensorLabelRemovedEvent
 * @description Describes the creation of a SensorLabel
 *
 * @property {string} subject The event subject
 * @property {ISensorLabelRemovedEventPayload} payload
 *
 */
export interface ISensorLabelRemovedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorLabelRemoved;
  payload: ISensorLabelRemovedEventPayload;
}
