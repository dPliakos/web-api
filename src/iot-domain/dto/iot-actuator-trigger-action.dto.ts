import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsOptional, IsUUID } from 'class-validator';
import { IoTValue, ResourceSearchQueryDTO } from '../../common';

export class IoTActuatorTriggerActionCreateDTO {
  @ApiProperty()
  @IsUUID()
  configuration: string;

  @ApiProperty()
  @IsUUID()
  alert: string;

  @ApiProperty()
  @IsIn(['normal', 'warning', 'critical', 'error'])
  previousState?: 'normal' | 'warning' | 'critical' | 'error';

  @ApiProperty()
  targetValue: IoTValue;
}

export class IoTActuatorTriggerActionUpdateDTO {
  @ApiProperty()
  @IsUUID()
  configuration: string;

  @ApiProperty()
  @IsIn(['normal', 'warning', 'critical', 'error'])
  previousState?: 'normal' | 'warning' | 'critical' | 'error';

  @ApiProperty()
  targetValue: IoTValue;
}

export class IoTActuatorTriggerActionQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty()
  @IsOptional()
  @IsUUID()
  identifier?: string;

  @ApiProperty()
  @IsUUID()
  @IsOptional()
  configuration?: string;
}
