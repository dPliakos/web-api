import { Injectable, Logger } from '@nestjs/common';
import { EntityService } from '../../../common';
import { SensorMeasurementEntity } from '../../entities/sensor-measurement.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SensorMeasurementService extends EntityService<SensorMeasurementEntity> {
  private readonly _logger = new Logger(
    `IoTIngress/${SensorMeasurementService.name}`,
  );

  constructor(
    @InjectRepository(SensorMeasurementEntity)
    private readonly _sensorMeasurementEntityRepository: Repository<SensorMeasurementEntity>,
  ) {
    super(SensorMeasurementEntity.name);
    this.repository = this._sensorMeasurementEntityRepository;
  }
}
