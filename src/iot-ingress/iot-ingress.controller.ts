import {
  Body,
  Controller,
  Delete,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  AddDemoDataDTO,
  IoTActuatorGetValue,
  IotDeviceIngresValueDTO,
  RemoveDemoDataDTO,
} from './dto/iot-device-ingress.dto';
import { IotAuthGuard } from './guards/iot-auth.guard';
import { ApiTags } from '@nestjs/swagger';
import { IotIngressService } from './services/iot-ingress.service';
import { InfluxdbService } from '../time-series/services/influxdb.service';
import { SensorService } from './services/sensor/sensor.service';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  IsDevModeGuard,
} from '../common';
import { SensorEntity } from './entities';
import { IotDeviceService } from './services/iot-device/iot-device.service';
import { IoTActuatorService } from './services/iot-actuator/iot-actuator.service';
import { SensorMeasurementService } from './services/sensor-measurement/sensor-measurement.service';
import { SensorMeasurementEntity } from './entities';
import { IoTActuatorEntity } from './entities';
import { FileInterceptor } from '@nestjs/platform-express';
import { ConfigService } from '@nestjs/config';
import { RabbitmqPublisherService } from './services/rabbitmq/rabbitmq-publisher.service';
import { IoTDeviceEntity } from './entities';

@ApiTags('iot-ingress')
@Controller('iot-ingress')
export class IotIngressController {
  private readonly _logger = new Logger(
    `IoTIngress/${IotIngressController.name}`,
  );

  constructor(
    private readonly _configService: ConfigService,
    private readonly _iotIngressService: IotIngressService,
    private readonly _influxDbService: InfluxdbService,
    private readonly _sensorService: SensorService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _iotDeviceService: IotDeviceService,
    private readonly _iotActuatorService: IoTActuatorService,
    private readonly _sensorMeasurementService: SensorMeasurementService,
    private readonly _rabbitMQPublisherService: RabbitmqPublisherService,
  ) {
    const env = this._configService.get('app.environment');
    if (env === 'dev') {
      setImmediate(() => {
        this._logger.warn(
          'Running in dev mode. Add and Delete demo data endpoints are available',
        );
      });
    }
  }

  @UseGuards(IotAuthGuard)
  @Post()
  async acceptValue(@Body() dto: IotDeviceIngresValueDTO) {
    let device: IoTDeviceEntity;

    try {
      device = await this._iotDeviceService.findOne({
        where: {
          identifier: dto.deviceIdentifier,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    await this._rabbitMQPublisherService.publishValue(
      device,
      dto.sensor,
      dto.value,
    );
    return {
      data: 'ok',
    };
  }

  @UseGuards(IotAuthGuard)
  @Post('/get-value')
  async getActuatorValue(@Body() dto: IoTActuatorGetValue) {
    let actuator: IoTActuatorEntity;
    let measurement: SensorMeasurementEntity;
    let value;

    try {
      const requests = [];
      requests.push(
        this._iotActuatorService.findOneOrThrow({
          where: {
            parentDevice: {
              identifier: dto.deviceIdentifier,
            },
            identifier: dto.actuator,
          },
        }),
      );

      requests.push(
        this._sensorMeasurementService.findOneOrThrow({
          where: {
            additionalType:
              SensorMeasurementEntity.AdditionalType.ActuatorMeasurement,
            actuator: {
              identifier: dto.actuator,
            },
          },
        }),
      );

      [actuator, measurement] = await Promise.all(requests);
    } catch (err) {
      this._logger.error(err?.stack ?? err);
      throw new InternalServerErrorException(err);
    }

    try {
      value = await this._iotIngressService.getLastValue(measurement);
    } catch (err) {
      this._logger.error(err?.stack ?? err);
      throw new InternalServerErrorException(err);
    }

    if (actuator.reportFormat === 'raw') {
      return value ?? null;
    } else {
      return {
        data: value ?? null,
      };
    }
  }

  @UseGuards(IsDevModeGuard)
  @Post('demo-data')
  @UseInterceptors(FileInterceptor('file'))
  async uploadDemoData(
    @Body() dto: AddDemoDataDTO,
    @UploadedFile() file: Express.Multer.File,
  ) {
    let sensor: SensorEntity;

    try {
      sensor = await this._sensorService.findSensor({
        where: {
          identifier: dto.sensor,
        },
        relations: ['measurements'],
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!sensor) {
      const error = new NotFoundException(
        `Sensor ${dto.sensor} could not be found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    const points = file.buffer.toString().split('\r\n');
    const targetDate = dto.anchorDate ?? new Date();
    const dates: Date[] = [];

    const currentPointDate = new Date(targetDate);
    currentPointDate.setHours(targetDate.getHours() - 4);
    for (let i = 0; i < points.length; i++) {
      dates.push(new Date(currentPointDate));
      currentPointDate.setMinutes(
        currentPointDate.getMinutes() + dto.timeStep ?? 20,
      );
    }

    await this._iotIngressService.storeBulk(points, dates, sensor, {
      demo: 'true',
    });
  }

  @UseGuards(IsDevModeGuard)
  @Delete('demo-data')
  async removeDemoData(@Body() dto: RemoveDemoDataDTO) {
    const params: Record<'sensor' | 'measurement', string> = {
      sensor: dto.sensor,
      measurement: dto.measurement,
    };

    await this._iotIngressService.removeBulk();
  }
}
