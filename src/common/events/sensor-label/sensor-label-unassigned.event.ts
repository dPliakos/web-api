import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface ISensorLabelUnassignedEventPayload
 * @description The payload of a ISensorLabelUnassignedEvent
 *
 * @property {string} label The public identifier of the SensorLabel
 * @property {string} sensor The public identifier of the target Sensor
 *
 */
interface ISensorLabelUnassignedEventPayload extends IApplicationEventPayload {
  label: string;
  sensor: string;
}

/**
 *
 * @interface ISensorLabelUnassignedEvent
 * @description Describes the creation of a SensorLabel
 *
 * @property {string} subject The event subject
 * @property {ISensorLabelUnassignedEventPayload} payload
 *
 */
export interface ISensorLabelUnassignedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorLabelAssigned;
  payload: ISensorLabelUnassignedEventPayload;
}
