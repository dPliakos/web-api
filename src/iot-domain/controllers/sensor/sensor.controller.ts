import {
  Controller,
  Get,
  UseGuards,
  Request,
  Post,
  Body,
  Put,
  Delete,
  Param,
  Logger,
  ConflictException,
  InternalServerErrorException,
  NotFoundException,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
  HttpCode,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import { SensorService } from '../../services/sensor/sensor.service';
import {
  CreateSensorInputDTO,
  SensorFilterQueryDTO,
  SensorRelatedLabelDTO,
  UpdateSensorInputDTO,
} from '../../dto/sensor.dto';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import { FindOptionsWhere, In, IsNull } from 'typeorm';
import { IotDeviceService } from '../../services/iot-device/iot-device.service';
import { SensorEntity } from '../../entities/sensor.entity';
import { SensorLabelEntity } from '../../entities/sensor-label.entity';
import { SensorLabelService } from '../../services/sensor-label/sensor-label.service';
import { MeasurementUnitService } from '../../services/measurement-unit/measurement-unit.service';
import { IotDeviceEntity } from 'src/iot-domain/entities/iot-device.entity';
import { MeasurementUnitEntity } from 'src/iot-domain/entities/measurement-unit.entity';

@Controller('sensor')
@ApiTags('sensor')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class SensorController {
  private readonly _logger = new Logger(`IoTDomain/${SensorController.name}`);

  constructor(
    private readonly _sensorService: SensorService,
    private readonly _iotDeviceService: IotDeviceService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _queryService: QueryService,
    private readonly _sensorLabelService: SensorLabelService,
    private readonly _measurementUnitService: MeasurementUnitService,
  ) {}

  @Get('/')
  async getUserSensors(
    @Request() req,
    @Query() paginationQuery: PaginationQueryDto,
    @Query() filtersQuery: SensorFilterQueryDTO,
  ): Promise<IResponse<SensorEntity[]>> {
    const { take, skip, page, perPage } =
      this._queryService.resolvePagination(paginationQuery);

    const { searchFactors } = this._queryService.resolveSearchQuery(
      filtersQuery,
      ['name', 'description'],
    );

    let sensors: SensorEntity[];
    let sensorCount: number;

    const whereClause: FindOptionsWhere<SensorEntity> = {
      owner: {
        id: req.user.id,
      },
      identifier: filtersQuery?.identifier,
      parentDevice: {
        identifier: filtersQuery?.device,
      },
      valueType: filtersQuery?.valueType,
    };

    if (filtersQuery.label) {
      filtersQuery.labels = [filtersQuery.label];
    }

    if (Array.isArray(filtersQuery?.labels)) {
      const labels = await this._sensorLabelService.findAll({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: In(filtersQuery.labels),
        },
      });

      if (labels?.length > 0) {
        whereClause.labels = {
          id: In(labels.map(({ id }) => id)),
        };
      }
    }

    const whereClauseWithSearch =
      this._sensorService.formatFindOptionsWithSearch(
        whereClause,
        searchFactors,
      );

    try {
      [sensors, sensorCount] = await this._sensorService.findAndCount({
        take,
        skip,
        where: whereClauseWithSearch,
        relations: [
          'parentDevice',
          'measurements',
          'measurementUnit',
          'labels',
        ],
        order: {
          createdAt: 'ASC',
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    return {
      data: sensors,
      total: sensorCount,
      page,
      perPage,
    };
  }

  @Get('/:identifier')
  async getSensor(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<SensorEntity>> {
    const sensor = await this._sensorService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
        deletedDate: null,
      },
      order: {
        createdAt: 'DESC',
      },
      relations: ['parentDevice', 'measurements', 'measurementUnit', 'labels'],
    });
    if (!sensor) {
      const notFoundError = new NotFoundException(
        `Sensor with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    return {
      data: sensor,
    };
  }

  @Post('/')
  async createSensor(
    @Request() req,
    @Body() dto: CreateSensorInputDTO,
  ): Promise<IResponse<SensorEntity>> {
    let device: IotDeviceEntity;
    let measurementUnit: MeasurementUnitEntity;

    try {
      const queries: (
        | Promise<IotDeviceEntity>
        | Promise<MeasurementUnitEntity>
      )[] = [
        this._iotDeviceService.findOne({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: dto.parentIoTDevice,
            deletedDate: IsNull(),
          },
        }),
        this._measurementUnitService.findOne({
          where: {
            owner: {
              id: In([global.publicUserId, req.user.id]),
            },
            identifier: dto.measurementUnit,
          },
        }),
      ];

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      [device, measurementUnit] = await Promise.all(queries);
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (!device) {
      const badRequestError = new ConflictException(
        `IoTDevice with identifier ${dto.parentIoTDevice} does not exist`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(badRequestError, badRequestError),
      );
    } else if (!measurementUnit) {
      const error = new NotFoundException(
        `MeasurementUnit with identifier ${dto.measurementUnit} does not exist`,
      );

      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    let sensor;

    try {
      sensor = await this._sensorService.save({
        owner: req.user,
        name: dto.name,
        description: dto.description,
        valueType: dto.valueType,
        measurementUnit: measurementUnit,
        parentDevice: device,
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    return {
      data: sensor,
    };
  }

  @Put('/:identifier')
  async updateSensor(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: UpdateSensorInputDTO,
  ) {
    const sensor = await this._sensorService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        deletedDate: null,
        identifier: params.identifier,
      },
      relations: ['labels'],
    });

    const updatedFields: Partial<SensorEntity> = {};

    if (sensor?.parentDevice?.identifier !== dto?.parentDevice?.identifier) {
      const newParentDevice = await this._iotDeviceService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto?.parentDevice?.identifier,
          deletedDate: null,
        },
      });

      if (!newParentDevice) {
        const conflictError = new ConflictException(
          `IoTDevice with identifier ${dto?.parentDevice?.identifier} does not exist`,
        );
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          conflictError,
        );
      } else {
        updatedFields.parentDevice = newParentDevice;
      }
    }

    const editableFields = ['name', 'description'];

    for (const field of editableFields) {
      if (dto.hasOwnProperty(field)) {
        updatedFields[field] = dto[field];
      }
    }

    if (Object.keys(updatedFields).length > 0) {
      const updateResults = await this._sensorService.update(
        sensor.id,
        updatedFields,
      );

      if (updateResults.affected <= 0) {
        const error = new InternalServerErrorException(
          `Could not update sensor with identifier ${sensor.identifier}`,
        );
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(error, error),
        );
      }
    }

    let measurementUnit: MeasurementUnitEntity;
    if (dto.measurementUnit) {
      try {
        measurementUnit = await this._measurementUnitService.findOne({
          where: {
            identifier: dto.measurementUnit,
            owner: {
              id: In([req.user.id, global.publicUserId]),
            },
          },
        });
      } catch (err) {
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(err, new InternalServerErrorException()),
        );
      }

      if (!measurementUnit) {
        const error = new NotFoundException(
          `MeasurementUnit ${dto.measurementUnit} was not found`,
        );
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(error, error),
        );
      }

      try {
        await this._sensorService.update(sensor.id, {
          measurementUnit: measurementUnit,
        });
      } catch (err) {
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(err, new InternalServerErrorException()),
        );
      }
    }

    if (Array.isArray(dto?.labels) && dto.labels.length > 0) {
      let labels: SensorLabelEntity[];
      let target: string[];

      if (typeof dto.labels[0] === 'string') {
        target = dto.labels;
      } else {
        target = dto.labels.map((l) => (l as any)?.identifier).filter((l) => l);
      }

      try {
        labels = await this._sensorLabelService.findAll({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: In(target),
          },
        });

        const refreshed = await this._sensorService.findOne({
          where: {
            id: sensor.id,
          },
        });

        await this._sensorService.updateLabels(refreshed, labels);
      } catch (err) {
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(err, new InternalServerErrorException()),
        );
      }
    }

    const updatedSensor = await this._sensorService.findOne({
      where: {
        id: sensor.id,
        owner: {
          id: req.user.id,
        },
      },
    });

    return {
      data: updatedSensor,
    };
  }

  @Delete('/:identifier')
  async removeSensor(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<SensorEntity>> {
    const sensor = await this._sensorService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
        deletedDate: null,
      },
    });

    if (!sensor) {
      const notFoundError = new NotFoundException(
        `Sensor with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        notFoundError,
      );
    }

    const deleteResult = await this._sensorService.softRemove(sensor.id);
    if (deleteResult.affected <= 0) {
      const error = new InternalServerErrorException(
        `Sensor with identifier ${params.identifier} could not be deleted`,
      );
      throw this._encapsulatedErrorService.formatError(this._logger, error);
    }

    const updatedSensor = await this._sensorService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      withDeleted: true,
    });

    return {
      data: updatedSensor,
    };
  }

  @Post('/:identifier/add-label')
  @HttpCode(200)
  async addLabel(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: SensorRelatedLabelDTO,
  ) {
    let sensor: SensorEntity;
    let label: SensorLabelEntity;

    try {
      const sensorRequest = this._sensorService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        relations: ['labels'],
      });

      const labelRequest = this._sensorLabelService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.label,
        },
      });

      [sensor, label] = await Promise.all([sensorRequest, labelRequest]);
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    let notFoundError;
    if (!sensor) {
      notFoundError = new NotFoundException(
        `Sensor ${params.identifier} was not found`,
      );
    } else if (!label) {
      notFoundError = new NotFoundException(
        `SensorLabel ${dto.label} was not found`,
      );
    }

    if (notFoundError) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    const exist = sensor.labels.find(({ id }) => id === label.id);

    if (exist) {
      const conflict = new ConflictException(
        `SensorLabel ${label.identifier} already exist in Sensor ${sensor.identifier}`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(conflict, conflict),
      );
    }

    await this._sensorService.addLabel(sensor, label);
    const updated = await this._sensorService.findOne({
      where: {
        id: sensor.id,
      },
      relations: ['labels', 'measurements', 'parentDevice'],
    });

    return {
      data: updated,
    };
  }

  @Post('/:identifier/remove-label')
  @HttpCode(200)
  async removeLabel(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: SensorRelatedLabelDTO,
  ) {
    let sensor: SensorEntity;
    let label: SensorLabelEntity;

    try {
      const sensorRequest = this._sensorService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        relations: ['labels'],
      });

      const labelRequest = this._sensorLabelService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.label,
        },
      });

      [sensor, label] = await Promise.all([sensorRequest, labelRequest]);
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    let notFoundError;
    if (!sensor) {
      notFoundError = new NotFoundException(
        `Sensor ${params.identifier} was not found`,
      );
    } else if (!label) {
      notFoundError = new NotFoundException(
        `SensorLabel ${dto.label} was not found`,
      );
    }

    if (notFoundError) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    const exist = sensor.labels.find(({ id }) => id === label.id);

    if (!exist) {
      const conflict = new ConflictException(
        `SensorLabel ${label.identifier} does not exist in Sensor ${sensor.identifier}`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(conflict, conflict),
      );
    }

    await this._sensorService.removeLabel(sensor, label);
    const updated = await this._sensorService.findOne({
      where: {
        id: sensor.id,
      },
      relations: ['labels', 'measurements', 'parentDevice'],
    });

    return {
      data: updated,
    };
  }
}
