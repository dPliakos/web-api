/**
 *
 * @interface EntityValidationResult
 * @description Describes the result of a validating an entity
 *
 * @property {boolean} isValid A flag for quickly determining whether an entity is valid
 * @property {Array<string>} validationErrors An array with the validation errors descriptions
 *
 */
export interface EntityValidationResult {
  isValid: boolean;
  validationErrors: string[];
}
