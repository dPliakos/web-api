import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';
import { ResourceSearchQueryDTO } from '../../common';
import { Transform } from 'class-transformer';

export class NotificationQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  @Transform((params) => params.value === true || params.value === 'true')
  acknowledged?: boolean;
}

export class NotificationUpdateDTO {
  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  @Transform((params) => params.value === true || params.value === 'true')
  acknowledged?: boolean;
}
