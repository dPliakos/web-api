import { IoTActuatorEntity } from './iot-actuator.entity';

describe('IoTDomain/IoTActuatorEntity', () => {
  it('should create', () => {
    expect(new IoTActuatorEntity()).toBeTruthy();
    expect(new IoTActuatorEntity().valueType).toBeUndefined();
    expect(new IoTActuatorEntity().reportFormat).toBeUndefined();
  });

  it('should create with default values', () => {
    const entity = new IoTActuatorEntity({});
    expect(entity.id).toBe(undefined);
    expect(entity.valueType).toBe('float');
    expect(entity.reportFormat).toBe('json');
  });

  it('should create with props', () => {
    const entity = new IoTActuatorEntity({
      id: 2,
      valueType: 'boolean',
      reportFormat: 'raw',
    });
    expect(entity.id).toBe(2);
    expect(entity.valueType).toBe('boolean');
    expect(entity.reportFormat).toBe('raw');
  });

  describe('validate limits', () => {
    it('should validate for number and undefined limits', () => {
      const entity = new IoTActuatorEntity({
        valueType: 'float',
      });
      expect(entity.validateLimits()).toBe(true);
      entity.valueType = 'int';
      expect(entity.validateLimits()).toBe(true);
    });

    it('should validate for number  and at least one numeric limit', () => {
      const entity = new IoTActuatorEntity();
      entity.valueType = 'float';
      entity.lowerLimit = 2;
      expect(entity.validateLimits()).toBe(true);
      entity.lowerLimit = undefined;
      entity.upperLimit = 2;
      expect(entity.validateLimits()).toBe(true);
    });

    it('should validate for number and valid numeric limits', () => {
      const entity = new IoTActuatorEntity();
      entity.valueType = 'float';
      entity.lowerLimit = 2;
      entity.upperLimit = 3;
      expect(entity.validateLimits()).toBe(true);
      entity.lowerLimit = 2;
      entity.upperLimit = 2;
      expect(entity.validateLimits()).toBe(true);
    });

    it('should fail for invalid limits', () => {
      const entity = new IoTActuatorEntity();
      entity.valueType = 'int';
      entity.lowerLimit = 3;
      entity.upperLimit = 2;
      expect(entity.validateLimits()).toBe(false);
      entity.lowerLimit = 6;
      entity.upperLimit = 2;
      expect(entity.validateLimits()).toBe(false);
    });

    it('should validate for non numeric valueType', () => {
      const entity = new IoTActuatorEntity();
      entity.valueType = 'string';
      entity.lowerLimit = 3;
      entity.upperLimit = 2;
      expect(entity.validateLimits()).toBe(true);
      entity.valueType = 'boolean';
      entity.lowerLimit = 6;
      entity.upperLimit = 2;
      expect(entity.validateLimits()).toBe(true);
    });
  });
});
