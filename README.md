# Open Smart Campus Network (OpenSCN)
## WEB API

## Documentation
You can read the [API documentation](https://openscn.gitlab.io/web-api/)

The web-api is the interface of the openscn to the users and admins. Contains the business logic of the service and acts as interface to all other services that the user should be able to use.

## Installation

#### Using Docker
- `nvm use`
- `npm install`
- `docker-compose up`


## Services

| name      | port |
|-----------|------|
| api       | 3000 |
| influx    | 8096 |
