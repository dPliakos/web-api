export enum ApplicationEventSubject {
  // IoTDevice
  IoTDeviceCreated = 'IoTDevice.created',
  IoTDeviceSoftRemoved = 'IoTDevice.softRemoved',
  IoTDeviceTokenCreated = 'IoTDeviceToken.created',
  IoTDeviceTokenUpdated = 'IoTDeviceToken.updated',
  IoTDeviceTokenRemoved = 'IoTDeviceToken.removed',
  SensorMeasurementCreated = 'SensorMeasurement.created',
  SensorMeasurementRemoved = 'SensorMeasurement.softRemoved',
  SensorMeasurementUpdated = 'SensorMeasurement.updated',
  SensorMeasurementActivated = 'SensorMeasurement.activated',
  SensorCreated = 'Sensor.created',
  SensorUpdated = 'Sensor.updated',
  SensorLabelCreated = 'SensorLabel.created',
  SensorLabelUpdated = 'SensorLabel.updated',
  SensorLabelRemoved = 'SensorLabel.softRemoved',
  SensorLabelAssigned = 'SensorLabel.assigned',
  SensorLabelUnassigned = 'SensorLabel.unassigned',
  ActuatorCreated = 'Actuator.created',
  ActuatorUpdated = 'Actuator.updated',
  ActuatorRemoved = 'Actuator.removed',
  IoTTaskAlertCheckResultReceived = 'IoTTaskAlertCheckResult.Received',
  IoTAlertCreated = 'IoTAlert.created',
  IoTAlertUpdated = 'IoTAlert.updated',
  UserCreated = 'User.created',
}

export type IApplicationEventPayload = Record<string, unknown>;

/**
 *
 * @interface IApplicationEvent
 * @description Describes an ApplicationEvent. Use it by extending it for specific events
 *
 * @property {ApplicationEventSubject} subject The event name that will be passed to the event emitter
 * @property {unknown} payload Pass the specific event when extending this interface
 *
 */
export interface IApplicationEvent {
  subject: ApplicationEventSubject;
  payload: Record<string, unknown> | IApplicationEventPayload;
}
