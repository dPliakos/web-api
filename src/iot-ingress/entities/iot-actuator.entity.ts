import { Column, DeepPartial, Entity, ManyToOne, OneToMany } from 'typeorm';
import { SensorMeasurementEntity } from './sensor-measurement.entity';
import { EntityValidationResult, IoTValue, Thing } from '../../common';
import { IoTDeviceEntity } from './iot-device.entity';

@Entity({
  name: 'IoTIngress_IoTActuator',
})
export class IoTActuatorEntity extends Thing {
  constructor(props?: DeepPartial<IoTActuatorEntity>) {
    super(props);

    if (props) {
      this.name = props.name;
      this.valueType = props.valueType;
      this.lowerLimit = props.lowerLimit;
      this.upperLimit = props.upperLimit;
      this.reportFormat = props.reportFormat;
      this.parentDevice = props.parentDevice as IoTDeviceEntity;
      this.measurements = props.measurements as SensorMeasurementEntity[];
    }
  }

  @Column()
  owner: string;

  @Column()
  name: string;

  @Column({
    nullable: false,
    default: 'float',
    enum: ['int', 'float', 'boolean', 'string'],
  })
  valueType: IoTValue;

  @Column({
    nullable: true,
  })
  lowerLimit?: number;

  @Column({
    nullable: true,
  })
  upperLimit?: number;

  @Column({
    default: 'json',
    enum: ['raw', 'json'],
  })
  reportFormat: 'raw' | 'json';

  @ManyToOne(() => IoTDeviceEntity, (device) => device.sensors)
  parentDevice: IoTDeviceEntity;

  @OneToMany(
    () => SensorMeasurementEntity,
    (measurement) => measurement.actuator,
  )
  measurements: SensorMeasurementEntity[];

  validate(): EntityValidationResult {
    const errors = [];
    const validLimits = this.validateLimits();
    const reportFormatOk =
      this.reportFormat === 'raw' || this.reportFormat === 'json';

    if (!validLimits) {
      errors.push(`Limits violation`);
    }

    if (!reportFormatOk) {
      errors.push('Report format must be either raw or json');
    }

    return {
      isValid: errors.length === 0,
      validationErrors: errors,
    };
  }

  validateLimits() {
    if (this.valueType === 'int' || this.valueType === 'float') {
      const lower = Number(this.lowerLimit);
      const upper = Number(this.upperLimit);

      return !isNaN(lower) && !isNaN(upper) ? lower <= upper : true;
    } else {
      return false;
    }
  }
}
