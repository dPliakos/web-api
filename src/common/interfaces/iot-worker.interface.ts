export enum IotWorkerJobsQueueEnum {
  '0.0.1' = 'openscn-iot-worker-jobs@0.0.1',
  '0.1.0' = 'openscn-iot-worker-jobs@0.1.0',
  Current = 'openscn-iot-worker-jobs@0.1.0',
  Alpha = 'openscn-iot-worker-jobs@alpha',
}

export enum IoTWorkerResultsQueueEnum {
  '0.0.1' = 'openscn-iot-worker-results@0.0.1',
  '0.1.0' = 'openscn-iot-worker-results@0.1.0',
  Current = 'openscn-iot-worker-results@0.1.0',
  Alpha = 'openscn-iot-worker-results@alpha',
}
