import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../interfaces';

/**
 *
 * @interface SensorMeasurementActivatedEventPayload
 *
 * @property {string} identifier The identifier of the target sensor
 * @property {string} activeMeasurement The identifier of the target SensorMeasurement
 *
 */
interface SensorMeasurementActivatedEventPayload
  extends IApplicationEventPayload {
  sensor?: string;
  actuator?: string;
  activeMeasurement: string;
}

/**
 *
 * @interface SensorMeasurementActivatedEvent
 * @extends IApplicationEvent
 * @description Describes an event that updates the active measurement of a sensor
 *
 */
export interface SensorMeasurementActivatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorMeasurementActivated;
  payload: SensorMeasurementActivatedEventPayload;
}
