import {
  Controller,
  Get,
  InternalServerErrorException,
  Logger,
  Query,
  UseGuards,
} from '@nestjs/common';
import { IoTQueryDTO, IoTValue, RawIoTValue } from '../dto/iot-query.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IotQueryService } from '../services/iot-query.service';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  IResponse,
} from '../../common';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../auth';
import { IotQueryGuard } from '../guards/iot-query-guard';

@ApiTags('iot-query')
@Controller('iot-query')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard, IotQueryGuard)
export class IotQueryController {
  private readonly _logger = new Logger(`IoTQuery/${IotQueryController.name}`);

  constructor(
    private readonly _iotQueryService: IotQueryService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
  ) {}

  @Get()
  async query(@Query() dto: IoTQueryDTO): Promise<IResponse<IoTValue[]>> {
    try {
      const rawData = (await this._iotQueryService.query(dto)) as RawIoTValue[];
      const data = this._iotQueryService.formatData(rawData);
      return {
        data,
      };
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }
  }
}
