import { Module } from '@nestjs/common';
import { CommonModule } from '../common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities';
import { UsersService } from './services/users.service';

@Module({
  controllers: [],
  imports: [CommonModule, TypeOrmModule.forFeature([User])],
  exports: [UsersService],
  providers: [UsersService],
})
export class UsersModule {
  constructor() {
    global.publicUserId = 1;
  }
}
