import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface ISensorLabelAssignedEventPayload
 * @description The payload of a ISensorLabelAssignedEvent
 *
 * @property {string} label The public identifier of the SensorLabel
 * @property {string} sensor The public identifier of the target Sensor
 *
 */
interface ISensorLabelAssignedEventPayload extends IApplicationEventPayload {
  label: string;
  sensor: string;
}

/**
 *
 * @interface ISensorLabelRemovedEvent
 * @description Describes the creation of a SensorLabel
 *
 * @property {string} subject The event subject
 * @property {ISensorLabelRemovedEventPayload} payload
 *
 */
export interface ISensorLabelAssignedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorLabelAssigned;
  payload: ISensorLabelAssignedEventPayload;
}
