import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { EntityService } from '../../../common';
import { Logger } from '@nestjs/common';
import { IotActuatorTriggerConfigurationEntity } from 'src/iot-domain/entities/iot-actuator-trigger-configuration.entity';

export class IotActuatorTriggerConfigurationService extends EntityService<IotActuatorTriggerConfigurationEntity> {
  private readonly _logger = new Logger(
    `IoTDomain/${IotActuatorTriggerConfigurationService.name}`,
  );

  constructor(
    @InjectRepository(IotActuatorTriggerConfigurationEntity)
    private readonly _iotActuatorTriggerConfigurationRepository: Repository<IotActuatorTriggerConfigurationEntity>,
  ) {
    super(IotActuatorTriggerConfigurationEntity.name);
    this.repository = _iotActuatorTriggerConfigurationRepository;
  }

  async create(item: DeepPartial<IotActuatorTriggerConfigurationEntity>) {
    const configuration = new IotActuatorTriggerConfigurationEntity(item);
    const validation = configuration.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    return configuration;
  }
}
