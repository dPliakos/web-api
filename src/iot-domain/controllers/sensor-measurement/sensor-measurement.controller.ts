import {
  BadRequestException,
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  BaseController,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import { SensorMeasurementEntity } from '../../entities/sensor-measurement.entity';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import { SensorMeasurementService } from '../../services/sensor-measeurement/sensor-measurement.service';
import {
  CreateSensorMeasurementDTO,
  SensorMeasurementQueryDTO,
  UpdateSensorMeasurementDTO,
} from '../../dto/sensor-measurement.dto';
import { SensorService } from '../../services/sensor/sensor.service';
import { IotActuatorService } from '../../services/iot-actuator/iot-actuator.service';

@Controller('sensor-measurement')
@ApiTags('sensor-measurement')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class SensorMeasurementController extends BaseController<SensorMeasurementEntity> {
  constructor(
    protected readonly _sensorMeasurementService: SensorMeasurementService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
    protected readonly _sensorService: SensorService,
    protected readonly _queryService: QueryService,
    private readonly _iotActuatorService: IotActuatorService,
  ) {
    super(
      _sensorMeasurementService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(`IoTDomain/${SensorMeasurementController.name}`),
    );
  }

  @Get()
  async list(
    @Request() req,
    @Query() paginationQuery: PaginationQueryDto,
    @Query() filtersQuery: SensorMeasurementQueryDTO,
  ): Promise<IResponse<SensorMeasurementEntity[]>> {
    return super.get(
      req,
      paginationQuery,
      filtersQuery,
      {
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: filtersQuery?.identifier,
          sensor: {
            identifier: filtersQuery?.sensor,
          },
          actuator: {
            identifier: filtersQuery?.actuator,
          },
          isActive: filtersQuery.isActive === 'true' ? true : undefined,
          isProduction: filtersQuery.isProduction === 'true' ? true : undefined,
        },
        order: {
          createdAt: 'DESC',
        },
        relations: ['owner', 'sensor', 'actuator'],
      },
      ['name', 'description'],
    );
  }

  @Get(':identifier')
  async getOne(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<SensorMeasurementEntity>> {
    return super.getOne(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      relations: {
        owner: true,
        sensor: { measurementUnit: true },
        actuator: true,
      },
    });
  }

  @Post()
  async create(@Request() req, @Body() dto: CreateSensorMeasurementDTO) {
    let sensor;
    let measurement;
    let actuator;

    if (!dto.sensor && !dto.actuator) {
      const error = new BadRequestException(
        `${this._sensorMeasurementService.name} must belong to a Sensor or an Actuator`,
      );
      throw this.getThrowable(error, error);
    } else if (dto.sensor && dto.actuator) {
      const error = new BadRequestException(
        `${this._sensorMeasurementService.name} must belong either to a Sensor or an Actuator`,
      );
      throw this.getThrowable(error, error);
    }

    try {
      if (dto.sensor) {
        sensor = await this._sensorService.findOneOrThrow({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: dto.sensor,
          },
        });
      } else if (dto.actuator) {
        actuator = await this._iotActuatorService.findOneOrThrow({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: dto.actuator,
          },
        });
      }
    } catch (err) {
      throw this.getThrowable(err, err);
    }

    try {
      measurement = await this._sensorMeasurementService.createAndSave({
        owner: req.user,
        name: dto.name,
        description: dto.description,
        isActive: dto.isActive,
        isProduction: dto.isProduction,
        sensor,
        actuator,
      });
    } catch (err) {
      this.getThrowable(err);
    }

    return {
      data: measurement,
    };
  }

  @Put(':identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: UpdateSensorMeasurementDTO,
  ) {
    let measurement;

    try {
      measurement = await this._sensorMeasurementService.findOneOrThrow({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        relations: ['sensor', 'owner', 'actuator'],
      });
    } catch (err) {
      this.getThrowable(err, err);
    }

    const updatedFields = this.filterUpdatableFields(dto, [
      'name',
      'description',
      'isActive',
    ]);
    const measurementEntity =
      await this._sensorMeasurementService.createAndSave({
        ...measurement,
        ...updatedFields,
      });

    return {
      data: measurementEntity,
    };
  }

  @Delete(':identifier')
  async remove(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.remove(req, params, {
      where: {
        identifier: params.identifier,
        owner: {
          id: req.user.id,
        },
      },
    });
  }
}
