import { InjectRepository } from '@nestjs/typeorm';
import { IoTAlertConfigurationEntity } from '../../entities/iot-alert-configuration.entity';
import { DeepPartial, FindOptionsWhere, Repository } from 'typeorm';
import { EntityService } from '../../../common';
import { Logger } from '@nestjs/common';
import { IoTTaskService } from '../../../iot-tasks';

export class IoTAlertConfigurationService extends EntityService<IoTAlertConfigurationEntity> {
  private readonly _logger = new Logger(
    `IoTDomain/${IoTAlertConfigurationService.name}`,
  );

  constructor(
    @InjectRepository(IoTAlertConfigurationEntity)
    private readonly _iotAlertConfigurationRepository: Repository<IoTAlertConfigurationEntity>,
    private readonly _iotTaskService: IoTTaskService,
  ) {
    super(IoTAlertConfigurationEntity.name);
    this.repository = _iotAlertConfigurationRepository;
  }

  async save(
    item:
      | DeepPartial<IoTAlertConfigurationEntity>
      | IoTAlertConfigurationEntity,
  ) {
    let result;
    await this.repository.manager.transaction(async () => {
      if (item.id) {
        const existing = await this.findOne({
          where: {
            id: item.id,
          },
          relations: ['owner', 'task'],
        });

        if (
          existing?.task &&
          existing?.task.enabled === true &&
          existing?.isActive === true &&
          item.isActive === false
        ) {
          await this._iotTaskService.save({
            ...existing.task,
            enabled: false,
          });
        }
      }

      result = await super.save(item);
    });

    return result;
  }

  async create(item: DeepPartial<IoTAlertConfigurationEntity>) {
    const configuration = new IoTAlertConfigurationEntity(item);
    const validation = configuration.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    return configuration;
  }

  async softRemove(id: number | FindOptionsWhere<IoTAlertConfigurationEntity>) {
    let result;
    await this.repository.manager.transaction(async () => {
      let alertConfiguration;
      if (typeof id === 'number') {
        alertConfiguration = await this.findOne({
          where: {
            id: id,
          },
          relations: ['task'],
        });
      } else {
        alertConfiguration = await this.findOne({
          where: id,
          relations: ['task'],
        });
      }

      if (alertConfiguration) {
        await this._iotTaskService.softRemove(alertConfiguration.task.id);
        result = await super.softRemove(id);
      }
    });

    return result;
  }
}
