import { Injectable } from '@nestjs/common';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EntityService,
  IActuatorCreatedEvent,
  IActuatorRemovedEvent,
  IActuatorUpdatedEvent,
  IoTValue,
} from '../../../common';
import { IoTActuatorEntity } from '../../entities';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { SensorLabelEntity } from '../../entities';
import { SensorMeasurementService } from '../sensor-measeurement/sensor-measurement.service';
import { InfluxdbService } from '../../../time-series/services/influxdb.service';
import { SensorMeasurementEntity } from '../../entities';
import { RabbitmqPublisherService } from '../rabbitmq/rabbitmq-publisher.service';

@Injectable()
export class IotActuatorService extends EntityService<IoTActuatorEntity> {
  constructor(
    @InjectRepository(IoTActuatorEntity)
    private readonly _iotActuatorRepository: Repository<IoTActuatorEntity>,
    private readonly _applicationEventsServie: ApplicationEventsService,
    private readonly _sensorMeasurementService: SensorMeasurementService,
    private readonly _influxDBservice: InfluxdbService,
    private readonly _rabbitmqPublisher: RabbitmqPublisherService,
  ) {
    super(IoTActuatorEntity.name);
    this.repository = _iotActuatorRepository;
  }

  async create(item: DeepPartial<IoTActuatorEntity>) {
    return new IoTActuatorEntity(item);
  }

  async save(item: IoTActuatorEntity) {
    const validation = item.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    const action = isNaN(item.id) ? 'create' : 'update';

    const result = await super.save(item);

    const eventPayload:
      | IActuatorCreatedEvent['payload']
      | IActuatorUpdatedEvent['payload'] = {
      actuator: result.identifier,
      name: result.name,
      reportFormat: result.reportFormat,
      lowerLimit: result.lowerLimit,
      upperLimit: result.upperLimit,
      version: result.version,
    };

    let event: IActuatorCreatedEvent | IActuatorUpdatedEvent;
    if (action === 'create') {
      event = {
        subject: ApplicationEventSubject.ActuatorCreated,
        payload: {
          ...eventPayload,
          owner: result.identifier,
          parentDevice: result.parentDevice.identifier,
          valueType: result.valueType,
          reportFormat: result.reportFormat,
        },
      };
    } else {
      event = {
        subject: ApplicationEventSubject.ActuatorUpdated,
        payload: eventPayload,
      };
    }

    this._applicationEventsServie.emit(event);

    if (action === 'create') {
      await this._sensorMeasurementService.createAndSave({
        owner: item.owner,
        name: 'default',
        isProduction: false,
        isActive: true,
        actuator: item,
        description: 'Default measurement',
      });
    }

    return result;
  }

  async softRemove(id: number) {
    const actuator = await this._iotActuatorRepository.findOne({
      where: {
        id: id,
      },
    });

    const result = await super.remove(id);

    if (result.affected > 0) {
      const event: IActuatorRemovedEvent = {
        subject: ApplicationEventSubject.ActuatorRemoved,
        payload: {
          actuator: actuator.identifier,
        },
      };

      this._applicationEventsServie.emit(event);
    }

    return result;
  }

  async setLabels(actuator: IoTActuatorEntity, labels: SensorLabelEntity[]) {
    actuator.labels = labels;

    const validation = actuator.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    return await this.save(actuator);
  }

  async postValue(actuator: IoTActuatorEntity, value: IoTValue) {
    await this._rabbitmqPublisher.publishValue(
      actuator.parentDevice,
      actuator.identifier,
      value,
      actuator.reportFormat,
    );

    return value;
  }

  async getValue(actuator: IoTActuatorEntity) {
    const measurement = await this._sensorMeasurementService.findOneOrThrow({
      where: {
        additionalType:
          SensorMeasurementEntity.AdditionalType.ActuatorMeasurement,
        actuator: {
          id: actuator.id,
        },
        isActive: true,
      },
    });

    const value = await this._influxDBservice.getLast({
      measurement: measurement.identifier,
    });

    return this._influxDBservice.formatData(value);
  }
}
