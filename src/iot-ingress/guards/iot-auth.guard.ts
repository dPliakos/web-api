import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { IotDeviceService } from '../services/iot-device/iot-device.service';

@Injectable()
export class IotAuthGuard implements CanActivate {
  constructor(private readonly _iotDeviceService: IotDeviceService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const body = context.switchToHttp().getRequest().body;
    if (!body.deviceIdentifier) {
      throw new BadRequestException(`deviceIdentifier is missing`);
    } else if (!body.accessToken) {
      throw new BadRequestException(`accessToken is missing`);
    }

    const deviceToken = await this._iotDeviceService.findToken(
      body.deviceIdentifier,
      body.accessToken,
    );

    if (!deviceToken) {
      throw new ForbiddenException(
        `DeviceToken ${deviceToken?.identifier} could not be found`,
      );
    }

    const req = context.switchToHttp().getRequest();
    req.deviceToken = deviceToken;
    return true;
  }
}
