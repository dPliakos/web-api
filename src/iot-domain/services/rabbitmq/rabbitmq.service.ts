import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { IConfiguration } from '../../../config/config.inteface';
import {
  IoTDeviceEntity,
  IoTDeviceTokenEntity,
} from '../../../iot-ingress/entities';
import { Injectable, Logger } from '@nestjs/common';
import { IotDeviceEntity } from '../../entities';
import { IotDeviceAccessTokenService } from '../iot-device-access-token/iot-device-access-token.service';

@Injectable()
export class RabbitmqService {
  private readonly _config: IConfiguration['rabbitMQ'];
  private readonly _logger: Logger = new Logger(RabbitmqService.name);

  constructor(
    private readonly _configService: ConfigService,
    private readonly _httpService: HttpService,
    private readonly _iotDeviceAccessTokenService: IotDeviceAccessTokenService,
  ) {
    this._config = this._configService.get('rabbitmq');
    this.compareStates().catch((err) => this._logger.error(err));
  }

  async createUser(device: IotDeviceEntity, accessToken: IoTDeviceTokenEntity) {
    return new Promise((resolve, reject) => {
      const path = `http://${this._config.host}:${this._config.httpPort}/api/users/${device.identifier}`;
      const subscription = this._httpService
        .put(
          path,
          {
            password: accessToken.token,
            tags: 'device',
            virtual_host: this._config.vhost,
          },
          {
            auth: {
              username: this._config.username,
              password: this._config.password,
            },
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
        .subscribe((value) => {
          subscription.unsubscribe();

          this.addPermissions(device)
            .then(() => this.addTopicPermissions(device))
            // .then(() => this.createQueue(device))
            // .then(() => this.createBinding(device))
            .then(() => {
              return resolve(value.data);
            })
            .catch((err) => {
              return reject(err);
            });
        });
    });
  }

  async addPermissions(device: IotDeviceEntity) {
    return new Promise((resolve, reject) => {
      const path = `http://${this._config.host}:${
        this._config.httpPort
      }/api/permissions/${encodeURIComponent('/')}/${device.identifier}`;
      const subscription = this._httpService
        .put(
          path,
          {
            configure: `mqtt-subscription-${device.identifier}`,
            write: `amq.topic|mqtt-subscription-${device.identifier}`,
            read: `amq.topic|mqtt-subscription-${device.identifier}`,
          },
          {
            auth: {
              username: this._config.username,
              password: this._config.password,
            },
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
        .subscribe((value) => {
          subscription.unsubscribe();
          if (value.status >= 400) {
            this._logger.error(value);
            return resolve(value);
          } else {
            return resolve(value);
          }
        });
    });
  }

  async addTopicPermissions(device: IotDeviceEntity) {
    return new Promise((resolve, reject) => {
      const path = `http://${this._config.host}:${
        this._config.httpPort
      }/api/topic-permissions/${encodeURIComponent('/')}/${device.identifier}`;
      const subscription = this._httpService
        .put(
          path,
          {
            exchange: 'amq.topic',
            configure: ``,
            write: `device\.{username}\.*|.*{username}/*`,
            read: `device\.{username}\.*|.*{username}/*`,
          },
          {
            auth: {
              username: this._config.username,
              password: this._config.password,
            },
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
        .subscribe((value) => {
          subscription.unsubscribe();
          if (value.status >= 400) {
            this._logger.error(value);
            return resolve(value);
          } else {
            return resolve(value);
          }
        });
    });
  }

  async removePermissions(device: IotDeviceEntity) {
    return new Promise((resolve, reject) => {
      const path = `http://${this._config.host}:${this._config.httpPort}/api/permissions/${this._config.vhost}/${device.identifier}`;
      const subscription = this._httpService
        .put(
          path,
          {
            write: null,
            read: null,
          },
          {
            auth: {
              username: this._config.username,
              password: this._config.password,
            },
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
        .subscribe((value) => {
          subscription.unsubscribe();
          return resolve(value);
        });
    });
  }

  async removeUser(identifier: string) {
    return new Promise((resolve) => {
      const path = `http://${this._config.host}:${this._config.httpPort}/api/users/bulk-delete`;
      const subscription = this._httpService
        .post(
          path,
          {
            users: [identifier],
          },
          {
            auth: {
              username: this._config.username,
              password: this._config.password,
            },
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
        .subscribe((value) => {
          subscription.unsubscribe();
          return resolve(value.data as any as { name: string; tags: string }[]);
        });
    });
  }

  async getRabbitMQUsers(): Promise<{ name: string; tags: string }[]> {
    //: Promise<{ name: string; tags: string[] }[]> {
    return new Promise((resolve) => {
      const path = `http://${this._config.host}:${this._config.httpPort}/api/users`;
      const subscription = this._httpService
        .get(path, {
          auth: {
            username: this._config.username,
            password: this._config.password,
          },
          headers: {
            'Content-Type': 'application/json',
          },
        })
        .subscribe((value) => {
          subscription.unsubscribe();
          return resolve(value.data as any as { name: string; tags: string }[]);
        });
    });
  }

  async createQueue(device: IotDeviceEntity) {
    return new Promise((resolve, reject) => {
      const mqttQueue = `mqtt-subscription-${device.identifier}`;
      const path = `http://${this._config.host}:${
        this._config.httpPort
      }/api/queues/${encodeURIComponent(this._config.vhost)}/${mqttQueue}`;
      const subscription = this._httpService
        .put(
          path,
          {
            durable: true,
          },
          {
            auth: {
              username: this._config.username,
              password: this._config.password,
            },
          },
        )
        .subscribe((response) => {
          subscription.unsubscribe();
          if (response.status < 400) {
            this._logger.log(`Created queue ${mqttQueue}`);
            return resolve(response.data);
          } else {
            return reject(response.data);
          }
        });
    });
  }

  async createBinding(device: IotDeviceEntity) {
    return new Promise((resolve, reject) => {
      const queue = `mqtt-subscription-${device.identifier}`;
      const path = `http://${this._config.host}:${
        this._config.httpPort
      }/api/bindings/${encodeURIComponent(
        this._config.vhost,
      )}/e/amq.topic/q/${queue}`;
      const subscription = this._httpService
        .post(
          path,
          {
            routing_key: `/device/${device.identifier}/#`,
          },
          {
            auth: {
              username: this._config.username,
              password: this._config.password,
            },
          },
        )
        .subscribe((response) => {
          subscription.unsubscribe();
          if (response.status < 400) {
            this._logger.log(
              `Created binding for amq.topic on ${this._config.mqttQueue}`,
            );
            return resolve(response.data);
          } else {
            return reject(response.data);
          }
        });
    });
  }

  async compareStates() {
    const [rabbitmqUsers, tokens] = await Promise.all([
      this.getRabbitMQUsers(),
      this._iotDeviceAccessTokenService.findAll({
        where: {
          isValid: true,
        },
        relations: ['device'],
      }),
    ]);

    const rabbitMQUsersMap = rabbitmqUsers.reduce((p, c) => {
      if (c.tags.includes('device')) {
        p[c.name] = c;
      }
      return p;
    }, {});

    const { devicesMap, tokensMap } = tokens.reduce(
      (p, c) => {
        p.devicesMap[c.device.identifier] = c.device;
        p.tokensMap[c.device.identifier] = c;
        return p;
      },
      {
        devicesMap: {},
        tokensMap: {},
      },
    );

    const shouldBeAddedToRabbitMQ: Promise<unknown>[] = Object.keys(devicesMap)
      .filter((key) => !rabbitMQUsersMap['key']) // NOTE: update all devices until a more finegraned solution is found
      .map((key) => this.createUser(devicesMap[key], tokensMap[key]));

    const shouldBeRemovedFromRabbitMQ: Promise<unknown>[] = Object.keys(
      rabbitMQUsersMap,
    )
      .filter((key) => !devicesMap[key])
      .map((key) => this.removeUser(key));

    Promise.allSettled([
      ...shouldBeRemovedFromRabbitMQ,
      ...shouldBeAddedToRabbitMQ,
    ])
      .then((result) => {
        const split = result.reduce(
          (p, c, i) => {
            if (c.status === 'fulfilled') {
              p.success++;
            } else {
              p.error.push(i);
            }

            return p;
          },
          {
            success: 0,
            error: [],
          },
        );

        this._logger.log(`Updated ${split.success} devices to RabbitMQ`);
        this._logger.log(
          `Occurred ${split.error.length} errors during RabbitMQ sync`,
        );
      })
      .catch((err) => {
        this._logger.error(err);
      });
  }
}
