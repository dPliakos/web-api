import { Injectable } from '@nestjs/common';
import { IoTQueryDTO, IoTValue, RawIoTValue } from '../dto/iot-query.dto';
import { InfluxdbService } from '../../time-series/services/influxdb.service';

@Injectable()
export class IotQueryService {
  constructor(private readonly _influxDbService: InfluxdbService) {}

  async query(params: IoTQueryDTO) {
    return this._influxDbService.query({
      measurement: params.measurement,
      start: params.start,
      stop: params.stop,
    });
  }

  formatData(data: RawIoTValue[]): IoTValue[] {
    if (Array.isArray(data)) {
      return data.map((value) => ({
        result: value.result,
        measurement: value._measurement,
        sensor_id: value.sensor_id,
        value:
          typeof value._value === 'boolean'
            ? value._value
              ? 1
              : 0
            : value._value,
        stop: value._stop,
        start: value._start,
        field: value._field,
        table: value.table,
        time: value._time,
      }));
    } else {
      return [];
    }
  }
}
