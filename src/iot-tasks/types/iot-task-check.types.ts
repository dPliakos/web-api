export enum IoTTaskThresholdCheckCondition {
  Greater = 'greater',
  Lower = 'lower',
}
