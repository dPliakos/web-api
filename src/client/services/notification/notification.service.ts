import { EntityService } from '../../../common';
import { NotificationEntity } from '../../entities/notification.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class NotificationService extends EntityService<NotificationEntity> {
  constructor(
    @InjectRepository(NotificationEntity)
    private readonly _repository: Repository<NotificationEntity>,
  ) {
    super(NotificationEntity.name);
    this.repository = _repository;
  }

  markAsAcknowledged(notifications: NotificationEntity[]) {
    const queries = [];
    for (let i = 0; i < notifications.length; i++) {
      queries.push(
        this._repository.save({
          ...notifications[i],
          acknowledged: true,
        }),
      );
    }

    return Promise.all(queries);
  }
}
