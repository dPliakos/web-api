import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsIn,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { IoTValue, ResourceSearchQueryDTO } from '../../common';

export class IoTActuatorTriggerConfigurationCreateDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsUUID()
  parent: string;

  @ApiProperty()
  @IsUUID()
  alert: string;

  @ApiProperty()
  @IsIn(['normal', 'warning', 'critical', 'error'])
  triggerState?: 'normal' | 'warning' | 'critical' | 'error';

  @ApiProperty()
  @IsNumber()
  targetValue: IoTValue;

  @ApiProperty()
  @IsBoolean()
  isActive: boolean;
}

export class IoTActuatorTriggerConfigurationUpdateDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsUUID()
  parent: string;

  @ApiProperty()
  @IsUUID()
  alert: string;

  @ApiProperty()
  @IsIn(['normal', 'warning', 'critical', 'error'])
  triggerState?: 'normal' | 'warning' | 'critical' | 'error';

  @ApiProperty()
  @IsNumber()
  targetValue: IoTValue;

  @ApiProperty()
  @IsBoolean()
  isActive: boolean;
}

export class IoTActuatorTriggerConfigurationQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty()
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty()
  @IsOptional()
  @IsUUID()
  identifier?: string;

  @ApiProperty()
  @IsUUID()
  @IsOptional()
  parent: string;

  @ApiProperty()
  @IsUUID()
  @IsOptional()
  alert: string;

  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  isActive: boolean;
}
