import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { EntityService } from '../../../common';
import { IoTTaskCheckEntity } from '../../entities';

@Injectable()
export class IoTTaskCheckService extends EntityService<IoTTaskCheckEntity> {
  constructor(
    @InjectRepository(IoTTaskCheckEntity)
    private readonly _iotTaskRepository: Repository<IoTTaskCheckEntity>,
  ) {
    super(IoTTaskCheckService.name);
    this.repository = _iotTaskRepository;
  }

  async create(item: DeepPartial<IoTTaskCheckEntity>) {
    const task = new IoTTaskCheckEntity(item);
    const validation = task.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    return task;
  }

  async createAndSave(item: DeepPartial<IoTTaskCheckEntity>) {
    const instance = await this.create(item);
    return this.save(instance);
  }
}
