import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface IIoTDeviceTokenCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTDeviceTokenCreated;
  payload: {
    identifier: string;
    token: string;
    isValid: boolean;
    device: string;
  };
}
