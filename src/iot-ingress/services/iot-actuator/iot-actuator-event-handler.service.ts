import { Injectable, Logger } from '@nestjs/common';
import { IoTActuatorService } from './iot-actuator.service';
import { OnEvent } from '@nestjs/event-emitter';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EventHandlerService,
  IActuatorCreatedEvent,
  IActuatorRemovedEvent,
  IActuatorUpdatedEvent,
  IApplicationEventPayload,
} from '../../../common';
import { IotDeviceService } from '../iot-device/iot-device.service';

@Injectable()
export class IoTActuatorEventHandlerService extends EventHandlerService {
  constructor(
    private readonly _actuatorService: IoTActuatorService,
    private readonly _iotDeviceService: IotDeviceService,
  ) {
    super(new Logger(`IoTIngress/${IoTActuatorEventHandlerService.name}`));
  }

  @OnEvent(ApplicationEventSubject.ActuatorCreated)
  async onActuatorCreatedEvent(event: IActuatorCreatedEvent['payload']) {
    this.logEvent(ApplicationEventSubject.ActuatorCreated, event);
    try {
      const device = await this._iotDeviceService.findOneOrThrow({
        where: {
          identifier: event.parentDevice,
        },
      });

      await this._actuatorService.save({
        identifier: event.actuator,
        parentDevice: device,
        reportFormat: event.reportFormat,
        valueType: event.valueType,
        lowerLimit: event.lowerLimit,
        upperLimit: event.upperLimit,
        name: event.name,
        owner: event.owner,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  @OnEvent(ApplicationEventSubject.ActuatorUpdated)
  async onActuatorUpdated(event: IActuatorUpdatedEvent['payload']) {
    this.logEvent(ApplicationEventSubject.ActuatorUpdated, event);
    try {
      const actuator = await this._actuatorService.findOneOrThrow({
        where: {
          identifier: event.actuator,
        },
      });

      await this._actuatorService.save({
        id: actuator.id,
        name: event.name,
        reportFormat: event.reportFormat,
        lowerLimit: event.lowerLimit,
        upperLimit: event.upperLimit,
        version: event.version,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  @OnEvent(ApplicationEventSubject.ActuatorRemoved)
  async onActuatorRemovedEvent(event: IActuatorRemovedEvent['payload']) {
    this.logEvent(ApplicationEventSubject.ActuatorRemoved, event);
    try {
      const actuator = await this._actuatorService.findOneOrThrow({
        where: {
          identifier: event.actuator,
        },
      });

      await this._actuatorService.softRemove(actuator.id);
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  logEvent(subject: ApplicationEventSubject, event: IApplicationEventPayload) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(subject, event),
    );
  }
}
