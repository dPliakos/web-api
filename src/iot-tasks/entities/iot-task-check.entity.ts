import { Column, DeepPartial, Entity } from 'typeorm';
import { EntityValidationResult } from '../../common';
import { IoTTaskConfigurationEntity } from './iot-task-configuration.entity';

/**
 *
 * @class IoTTaskCheckEntity
 * @extends OwnedThing
 * @description Describes a check task for iot sensors
 *
 * @property {string} additionalType The specific type of task
 * @property {number} times How many times the check should be violated before an alert is being published
 *
 */
@Entity({
  name: 'IoTTask_IoTTaskCheckEntity',
})
export class IoTTaskCheckEntity extends IoTTaskConfigurationEntity {
  constructor(props?: DeepPartial<IoTTaskCheckEntity>) {
    super(props);

    if (props) {
      this.times = props.times;
      this.additionalType = props.additionalType;
    }
  }

  @Column({
    nullable: false,
  })
  additionalType?: string;

  @Column({
    nullable: false,
    default: 1,
  })
  times: number;

  validate(): EntityValidationResult {
    const validation = super.validate();

    if (this.times <= 0) {
      validation.validationErrors.push(
        `Property times must be a positive integer and >= 0`,
      );
    }

    if (!this.additionalType) {
      validation.validationErrors.push(
        `Property 'additionalType' can not be empty`,
      );
    }

    return {
      isValid: validation.validationErrors.length === 0,
      validationErrors: validation.validationErrors,
    };
  }
}
