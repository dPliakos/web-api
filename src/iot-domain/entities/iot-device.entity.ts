import { Column, Entity, OneToMany } from 'typeorm';
import { SensorEntity } from './sensor.entity';
import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { IoTActuatorEntity } from './iot-actuator.entity';

/**
 *
 * @class IoTDevice
 * @description Declares web enabled device that contains sensor and is responsible for their authentication
 *
 * @property {string} name The name of the platform
 * @property {string} description A brief description of the platform's usage
 * @property {boolean} isActive Describes whether the authentication method is active by the user
 * @property {SensorEntity[]} sensors The list of attached sensors
 *
 */
@Entity({
  name: 'IoTDomain_IoTDevice',
})
export class IotDeviceEntity extends OwnedThing {
  @Column({
    nullable: false,
  })
  name: string;

  @Column({
    nullable: false,
  })
  description?: string;

  @OneToMany(() => SensorEntity, (sensor) => sensor.parentDevice)
  sensors: SensorEntity[];

  @OneToMany(() => IoTActuatorEntity, (actuator) => actuator.parentDevice)
  actuators: IoTActuatorEntity[];
}
