import {
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
  Request,
  Query,
  Logger,
  InternalServerErrorException,
  Param,
  NotFoundException,
  Body,
  ForbiddenException,
  BadRequestException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import {
  BaseController,
  EncapsulatedError,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import {
  MeasurementUnitCreateDTO,
  MeasurementUnitQueryDTO,
  MeasurementUnitUpdateDTO,
} from '../../dto/measurement-unit.dto';
import { MeasurementUnitService } from '../../services/measurement-unit/measurement-unit.service';
import { MeasurementUnitEntity } from '../../entities/measurement-unit.entity';
import { DeepPartial, In, UpdateResult } from 'typeorm';

@Controller('measurement-unit')
@ApiTags('measurement-unit')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class MeasurementUnitController extends BaseController<MeasurementUnitEntity> {
  constructor(
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
    protected readonly _measurementUnitService: MeasurementUnitService,
    protected readonly _queryService: QueryService,
  ) {
    super(
      _measurementUnitService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(`IoTDomain/${MeasurementUnitController}`),
    );
  }

  @Get()
  async get(
    @Request() req,
    @Query() pagination: PaginationQueryDto,
    @Query() query: MeasurementUnitQueryDTO,
  ) {
    return super.get(req, pagination, query, {
      where: {
        owner: {
          id: In([global.publicUserId, req.user.id]),
        },
        identifier: query?.identifier,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  @Get('/:identifier')
  async getOne(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<MeasurementUnitEntity>> {
    return super.getOne(req, params, {
      where: {
        owner: {
          id: In([global.publicUserId, req.user.id]),
        },
        identifier: params.identifier,
      },
    });
  }

  @Post()
  async create(@Request() req, @Body() dto: MeasurementUnitCreateDTO) {
    try {
      const result = await this._measurementUnitService.save({
        owner: req.user,
        name: dto.name,
        description: dto.description,
        symbol: dto.symbol,
      });

      return {
        data: result,
      };
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }
  }

  @Put('/:identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: MeasurementUnitUpdateDTO,
  ) {
    let unit: MeasurementUnitEntity;

    try {
      unit = await this._measurementUnitService.findOne({
        where: {
          owner: {
            id: In([global.publicUserId, req.user.id]),
          },
          identifier: params.identifier,
        },
        relations: ['owner'],
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!unit) {
      const error = new NotFoundException(
        `MeasurementUnit ${params?.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    } else if (unit.owner.id !== req.user.id) {
      const error = new ForbiddenException(
        `MeasurementUnit ${params?.identifier} is not updatable`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    const updatedFields: DeepPartial<MeasurementUnitEntity> = {};
    const editableFields = ['name', 'description', 'symbol'];

    for (const field of editableFields) {
      if (dto.hasOwnProperty(field)) {
        updatedFields[field] = dto[field];
      }
    }

    if (Object.keys(updatedFields).length === 0) {
      const error = new BadRequestException(
        `Update ${params.identifier} failed. No fields to update`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    let results: UpdateResult;
    try {
      results = await this._measurementUnitService.update(
        unit.id,
        updatedFields,
      );
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (results.affected === 0) {
      const error = new InternalServerErrorException(
        `Update ${params.identifier} failed. No rows affected`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    } else {
      const updated = await this._measurementUnitService.findOne({
        where: {
          id: unit.id,
        },
      });

      return {
        data: updated,
      };
    }
  }

  @Delete('/:identifier')
  async remote(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.remove(req, params, {
      where: {
        owner: {
          id: In([global.publicUserId, req.user.id]),
        },
        identifier: params.identifier,
      },
      relations: ['owner'],
    });
  }
}
