import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface ISensorLabelCreatedEventPayload
 * @description The payload of a ISensorLabelCreatedEvent
 *
 * @property {string} label The public identifier of the SensorLabel
 * @property {string} name The name of the SensorLabel
 * @property {string} owner The public identifier of the sensor owner
 *
 */
interface ISensorLabelCreatedEventPayload extends IApplicationEventPayload {
  label: string;
  name: string;
  owner: string;
}

/**
 *
 * @interface ISensorLabelCreatedEvent
 * @description Describes the creation of a SensorLabel
 *
 * @property {string} subject The event subject
 *
 */
export interface ISensorLabelCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorLabelCreated;
  payload: ISensorLabelCreatedEventPayload;
}
