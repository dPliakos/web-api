import { Thing } from '../../common';
import { Column, Entity } from 'typeorm';

@Entity({
  name: 'IoTIngress_DeviceToken',
})
export class IoTDeviceTokenEntity extends Thing {
  @Column({
    nullable: true,
  })
  isValid: boolean;

  @Column({
    nullable: true,
  })
  device: string;

  @Column({
    nullable: true,
  })
  token: string;
}
