import {
  BadRequestException,
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { SensorLabelService } from '../../services/sensor-label/sensor-label.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import {
  BaseController,
  EncapsulatedError,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import {
  SensorLabelCreateDTO,
  SensorLabelQueryDTO,
  SensorLabelUpdateDTO,
} from '../../dto/sensor-label.dto';
import { SensorLabelEntity } from '../../entities/sensor-label.entity';
import { DeepPartial, UpdateResult } from 'typeorm';

@Controller('sensor-label')
@ApiTags('sensor-label')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class SensorLabelController extends BaseController<SensorLabelEntity> {
  constructor(
    protected readonly _sensorLabelService: SensorLabelService,
    protected readonly _queryService: QueryService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
  ) {
    super(
      _sensorLabelService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(`IoTDomain/${SensorLabelController.name}`),
    );
  }

  @Get()
  async get(
    @Request() req,
    @Query() paginationQuery: PaginationQueryDto,
    @Query() filtersQuery: SensorLabelQueryDTO,
  ): Promise<IResponse<SensorLabelEntity[]>> {
    return super.get(
      req,
      paginationQuery,
      filtersQuery,
      {
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: filtersQuery?.identifier,
          name: filtersQuery?.name,
          additionalName: filtersQuery.additionalName,
        },
        order: {
          createdAt: 'ASC',
        },
      },
      ['name', 'description', 'additionalName'],
    );
  }

  @Get('/:identifier')
  async getOne(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<SensorLabelEntity>> {
    let sensorLabel: SensorLabelEntity;

    try {
      sensorLabel = await this._sensorLabelService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!sensorLabel) {
      const error = new NotFoundException(
        `SensorLabel ${params?.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    } else {
      return {
        data: sensorLabel,
      };
    }
  }

  @Put('/:identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: SensorLabelUpdateDTO,
  ): Promise<IResponse<SensorLabelEntity>> {
    let sensorLabel: SensorLabelEntity;

    try {
      sensorLabel = await this._sensorLabelService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!sensorLabel) {
      const error = new NotFoundException(
        `SensorLabel ${params?.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    const updatedFields: DeepPartial<SensorLabelEntity> = {};
    const editableFields = ['name', 'description'];

    for (const field of editableFields) {
      if (dto.hasOwnProperty(field)) {
        updatedFields[field] = dto[field];
      }
    }

    if (Object.keys(updatedFields).length === 0) {
      const error = new BadRequestException(
        `Update ${params.identifier} failed. No fields to update`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    let results: UpdateResult;
    try {
      results = await this._sensorLabelService.update(
        sensorLabel.id,
        updatedFields,
      );
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (results.affected === 0) {
      const error = new InternalServerErrorException(
        `Update ${params.identifier} failed. No rows affected`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    } else {
      const updated = await this._sensorLabelService.findOne({
        where: {
          id: sensorLabel.id,
        },
      });

      return {
        data: updated,
      };
    }
  }

  @Post()
  async create(
    @Request() req,
    @Body() dto: SensorLabelCreateDTO,
  ): Promise<IResponse<SensorLabelEntity>> {
    try {
      const result = await this._sensorLabelService.save({
        owner: req.user,
        name: dto.name,
        description: dto.description,
      });

      return {
        data: result,
      };
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }
  }

  @Delete('/:identifier')
  async remove(@Request() req, @Param() params: EntityIdentifierDTO) {
    let sensorLabel: SensorLabelEntity;

    try {
      sensorLabel = await this._sensorLabelService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!sensorLabel) {
      const error = new NotFoundException(
        `SensorLabel ${params?.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    } else {
      const result = await this._sensorLabelService.softRemove({
        id: sensorLabel.id,
      });

      if (result.affected === 0) {
        const error = new InternalServerErrorException(
          `SensorLabel ${params?.identifier} could not be deleted`,
        );
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(error, error),
        );
      } else {
        sensorLabel = await this._sensorLabelService.findOne({
          where: {
            id: sensorLabel.id,
          },
          withDeleted: true,
        });

        return {
          data: sensorLabel,
        };
      }
    }
  }
}
