export interface InfluxDBOptions {
  url: string;
  bucket: string;
  org: string;
  token: string;
}

/**
 *
 * @interface InfluxEntry
 * @description An influx entry is an a record to the influx database
 * For more information, please visit https://docs.influxdata.com/influxdb/v2.5/get-started/write/
 *
 * @property {string} measurement The name of the measurement
 * @property {string} sensorId The id of the sensor
 * @property {Array<Record<string, string>>} tags A list of tags to give that the measurement has
 * @property {Array<Record<string, string | number | boolean>>} fields The list of the values and their names
 * @property {number} timestamp Unix timestamp in second precision
 *
 */
export interface InfluxEntry {
  measurement: string;
  targetId?: string;
  type: string;
  tags: Record<string, string>[];
  fields: Record<string, string | number | boolean>[];
  timestamp: number;
}

export interface InfluxQuery {
  sensorId?: string;

  measurement: string;

  fields?: Record<string, 'string' | 'float' | 'int' | 'boolean'>[];

  start?: string | number;

  stop?: string | number;
}
