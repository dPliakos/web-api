import { Column, Entity } from 'typeorm';
import { Thing } from '../../common';

@Entity({
  name: 'IoTQuery_SensorMeasurement',
})
export class SensorMeasurementEntity extends Thing {
  private static _AdditionalType = {
    SensorMeasurement: 'SensorMeasurement',
    ActuatorMeasurement: 'ActuatorMeasurement',
  };

  static get AdditionalType() {
    return this._AdditionalType;
  }
  @Column({
    nullable: true,
  })
  owner: string;

  @Column({
    nullable: true,
    enum: [
      SensorMeasurementEntity.AdditionalType.SensorMeasurement,
      SensorMeasurementEntity.AdditionalType.ActuatorMeasurement,
    ],
  })
  additionalType?: string;

  @Column({
    nullable: true,
  })
  sensor?: string;

  @Column({
    nullable: true,
  })
  actuator?: string;

  @Column({
    default: true,
  })
  isActive: boolean;

  @Column({
    default: false,
  })
  isProduction: boolean;
}
