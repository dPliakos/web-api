import { Processor, OnWorkerEvent } from '@nestjs/bullmq';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  BaseProcessor,
  IoTTaskAlertCheckResultReceivedEvent,
  IoTWorkerResultsQueueEnum,
} from '../../../common';
import { Job } from 'bullmq';
import { Logger } from '@nestjs/common';

@Processor(IoTWorkerResultsQueueEnum.Current)
export class AlertCheckProcessor extends BaseProcessor {
  constructor(
    private readonly _applicationEventsService: ApplicationEventsService,
  ) {
    super(new Logger(AlertCheckProcessor.name));
  }

  async process(job: Job): Promise<any> {
    const event: IoTTaskAlertCheckResultReceivedEvent = {
      subject: ApplicationEventSubject.IoTTaskAlertCheckResultReceived,
      payload: {
        jobId: job.id,
        status: job.data.payload.status,
        prevStatus: job.data.payload.prevStatus,
        workerVersion: job.data.version,
        task: job.data.payload.task,
        queueName: job.data.queueName,
        error: job.data.error,
        value: job.data.payload.value,
      },
    };
    this._applicationEventsService.emit(event);

    return new Promise((resolve) => resolve(undefined));
  }

  @OnWorkerEvent('ready')
  onReady() {
    this._logger.verbose(
      `Ready - Listening on ${IoTWorkerResultsQueueEnum.Current}`,
    );
  }

  @OnWorkerEvent('active')
  onActive(job: Job) {
    this._logger.verbose(`Active - ${job.id}`);
  }

  @OnWorkerEvent('completed')
  onCompleted(job: Job) {
    this._logger.verbose(`Completed - ${job.id}`);
  }
}
