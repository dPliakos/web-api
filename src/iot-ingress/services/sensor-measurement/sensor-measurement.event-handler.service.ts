import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventSubject,
  EventHandlerService,
  SensorMeasurementCreatedEvent,
  SensorMeasurementRemovedEvent,
  SensorMeasurementUpdatedEvent,
} from '../../../common';
import { OnEvent } from '@nestjs/event-emitter';
import { SensorEntity } from '../../entities/sensor.entity';
import { SensorService } from '../sensor/sensor.service';
import { SensorMeasurementService } from './sensor-measurement.service';
import { IoTActuatorEntity } from '../../entities/iot-actuator.entity';
import { IoTActuatorService } from '../iot-actuator/iot-actuator.service';

@Injectable()
export class SensorMeasurementEventHandlerService extends EventHandlerService {
  constructor(
    private readonly _sensorService: SensorService,
    private readonly _sensorMeasurementService: SensorMeasurementService,
    private readonly _actuatorService: IoTActuatorService,
  ) {
    super(
      new Logger(`IoTIngress/${SensorMeasurementEventHandlerService.name}`),
    );
  }

  @OnEvent(ApplicationEventSubject.SensorMeasurementCreated)
  async onSensorMeasurementCreated(
    event: SensorMeasurementCreatedEvent['payload'],
  ) {
    this.logEvent(ApplicationEventSubject.SensorMeasurementCreated, event);

    let sensor: SensorEntity;
    let actuator: IoTActuatorEntity;
    try {
      if (event.sensor) {
        sensor = await this._sensorService.findSensor({
          where: {
            identifier: event.sensor,
          },
        });
      }

      if (event.actuator) {
        actuator = await this._actuatorService.findOne({
          where: {
            identifier: event.actuator,
          },
        });
      }

      await this._sensorMeasurementService.save({
        identifier: event.measurement,
        isProduction: event.isProduction,
        isActive: event.isActive,
        version: event.version,
        additionalType: event.additionalType,
        sensor: sensor,
        actuator: actuator,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  @OnEvent(ApplicationEventSubject.SensorMeasurementRemoved)
  async onSensorMeasurementRemoved(
    event: SensorMeasurementRemovedEvent['payload'],
  ) {
    this.logEvent(ApplicationEventSubject.SensorMeasurementRemoved, event);
    try {
      await this._sensorMeasurementService.softRemove({
        identifier: event.measurement,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  @OnEvent(ApplicationEventSubject.SensorMeasurementUpdated)
  async onSensorMeasurementUpdated(
    event: SensorMeasurementUpdatedEvent['payload'],
  ) {
    this.logEvent(ApplicationEventSubject.SensorMeasurementUpdated, event);

    let measurement;

    try {
      measurement = await this._sensorMeasurementService.findOne({
        where: {
          identifier: event.measurement,
        },
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }

    if (!measurement) {
      this._logger.error(
        `${this._sensorMeasurementService.name} with identifier ${event.measurement} was not found`,
      );
      return;
    } else if (measurement.version !== event.version - 1) {
      this._logger.error(
        `${this._sensorMeasurementService.name} with identifier ${measurement.identifier} updates from version ${measurement.version} to ${event.version}`,
      );
    }

    try {
      await this._sensorMeasurementService.save({
        id: measurement.id,
        isActive: event.isActive,
        isProduction: event.isProduction,
        version: event.version,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }
}
