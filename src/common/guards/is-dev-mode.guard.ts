import { CanActivate, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class IsDevModeGuard implements CanActivate {
  constructor(private readonly _configService: ConfigService) {}

  canActivate(): boolean {
    const env = this._configService.get('app.environment');
    return env === 'dev';
  }
}
