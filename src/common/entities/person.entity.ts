import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Thing } from './thing.entity';

/**
 *
 * @class Person
 * @description Describes a person's information
 *
 * @property {string} familyName The last name of the person
 * @property {string} givenName The first name of the person
 *
 */
@Entity({
  name: 'Common_Person',
})
export class Person extends Thing {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({
    nullable: true,
  })
  familyName?: string;

  @Column({
    nullable: true,
  })
  givenName?: string;
}
