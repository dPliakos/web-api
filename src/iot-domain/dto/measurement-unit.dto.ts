import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ResourceSearchQueryDTO } from '../../common';

export class MeasurementUnitQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  identifier?: string;
}

export class MeasurementUnitCreateDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty({
    required: false,
  })
  description?: string;

  @ApiProperty()
  @IsNotEmpty()
  symbol: string;
}

export class MeasurementUnitUpdateDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  description?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNotEmpty()
  symbol: string;
}
