import { Controller, Get } from '@nestjs/common';
import { IResponse } from './common';

@Controller()
export class AppController {
  @Get()
  async getHello(): Promise<IResponse<{ status: string }>> {
    return {
      data: {
        status: 'ok',
      },
    };
  }
}
